#include "classes.h"

#include <iostream>

SimpleClass::SimpleClass() {
    std::cout << "SimpleClass constructor" << std::endl;
}

SimpleClass::SimpleClass(SimpleClass const& other) {
    std::cout << "SimpleClass copy constructor" << std::endl;
}

SimpleClass& SimpleClass::operator=(SimpleClass const& other) {
    std::cout << "SimpleClass copy assignment operator" << std::endl;
    return *this;
}

SimpleClass::SimpleClass(SimpleClass&& other) noexcept {
    std::cout << "SimpleClass move constructor" << std::endl;
}

SimpleClass& SimpleClass::operator=(SimpleClass&& other) noexcept {
    std::cout << "SimpleClass move assignment operator" << std::endl;
    return *this;
}

SimpleClass::~SimpleClass() {
    std::cout << "SimpleClass destructor" << std::endl;
}


BaseClass::BaseClass() {
    std::cout << "BaseClass constructor" << std::endl;
}

BaseClass::BaseClass(BaseClass const& other) {
    std::cout << "BaseClass copy constructor" << std::endl;
}

BaseClass& BaseClass::operator=(BaseClass const& other) {
    std::cout << "BaseClass copy assignment operator" << std::endl;
    return *this;
}

BaseClass::BaseClass(BaseClass&& other) noexcept {
    std::cout << "BaseClass move constructor" << std::endl;
}

BaseClass& BaseClass::operator=(BaseClass&& other) noexcept {
    std::cout << "BaseClass move assignment operator" << std::endl;
    return *this;
}

BaseClass::~BaseClass() {
    std::cout << "BaseClass destructor" << std::endl;
}


AnotherBaseClass::AnotherBaseClass() {
    std::cout << "AnotherBaseClass constructor" << std::endl;
}

AnotherBaseClass::AnotherBaseClass(AnotherBaseClass const& other) {
    std::cout << "AnotherBaseClass copy constructor" << std::endl;
}

AnotherBaseClass& AnotherBaseClass::operator=(AnotherBaseClass const& other) {
    std::cout << "AnotherBaseClass copy assignment operator" << std::endl;
    return *this;
}

AnotherBaseClass::AnotherBaseClass(AnotherBaseClass&& other) noexcept {
    std::cout << "AnotherBaseClass move constructor" << std::endl;
}

AnotherBaseClass& AnotherBaseClass::operator=(AnotherBaseClass&& other) noexcept {
    std::cout << "AnotherBaseClass move assignment operator" << std::endl;
    return *this;
}

AnotherBaseClass::~AnotherBaseClass() {
    std::cout << "AnotherBaseClass destructor" << std::endl;
}


DerivedClass::DerivedClass() {
    std::cout << "DerivedClass constructor" << std::endl;
}

//TODO
DerivedClass::DerivedClass(DerivedClass const& other) {
    std::cout << "DerivedClass copy constructor" << std::endl;
}

//TODO
DerivedClass& DerivedClass::operator=(DerivedClass const& other) {
    std::cout << "DerivedClass copy assignment operator" << std::endl;
    return *this;
}

//TODO
DerivedClass::DerivedClass(DerivedClass&& other) noexcept {
    std::cout << "DerivedClass move constructor" << std::endl;
}

//TODO
DerivedClass& DerivedClass::operator=(DerivedClass&& other) noexcept {
    std::cout << "DerivedClass move assignment operator" << std::endl;
    return *this;
}

DerivedClass::~DerivedClass() {
    std::cout << "DerivedClass destructor" << std::endl;
}


MemberClass::MemberClass() {
    std::cout << "MemberClass constructor" << std::endl;
}

MemberClass::MemberClass(MemberClass const& other) {
    std::cout << "MemberClass copy constructor" << std::endl;
}

MemberClass& MemberClass::operator=(MemberClass const& other) {
    std::cout << "MemberClass copy assignment operator" << std::endl;
    return *this;
}

MemberClass::MemberClass(MemberClass&& other) noexcept {
    std::cout << "MemberClass move constructor" << std::endl;
}

MemberClass& MemberClass::operator=(MemberClass&& other) noexcept {
    std::cout << "MemberClass move assignment operator" << std::endl;
    return *this;
}

MemberClass::~MemberClass() {
    std::cout << "MemberClass destructor" << std::endl;
}


AnotherMemberClass::AnotherMemberClass() {
    std::cout << "AnotherMemberClass constructor" << std::endl;
}

AnotherMemberClass::AnotherMemberClass(AnotherMemberClass const& other) {
    std::cout << "AnotherMemberClass copy constructor" << std::endl;
}

AnotherMemberClass& AnotherMemberClass::operator=(AnotherMemberClass const& other) {
    std::cout << "AnotherMemberClass copy assignment operator" << std::endl;
    return *this;
}

AnotherMemberClass::AnotherMemberClass(AnotherMemberClass&& other) noexcept {
    std::cout << "AnotherMemberClass move constructor" << std::endl;
}

AnotherMemberClass& AnotherMemberClass::operator=(AnotherMemberClass&& other) noexcept {
    std::cout << "AnotherMemberClass move assignment operator" << std::endl;
    return *this;
}

AnotherMemberClass::~AnotherMemberClass() {
    std::cout << "AnotherMemberClass destructor" << std::endl;
}


CompositeClass::CompositeClass()
    : anotherMemberClass(), //order of declaration is what counts, so be careful when writing initialization lists
      BaseClass(),
      memberClass() {
    std::cout << "CompositeClass constructor" << std::endl;
}

CompositeClass::CompositeClass(CompositeClass const& other)
    : BaseClass(other),
      memberClass(other.memberClass),
      anotherMemberClass(other.anotherMemberClass) {
    std::cout << "CompositeClass copy constructor" << std::endl;
}

CompositeClass& CompositeClass::operator=(CompositeClass const& other) {
    std::cout << "CompositeClass copy assignment operator" << std::endl;
    BaseClass::operator=(other);
    memberClass = other.memberClass;
    anotherMemberClass = other.anotherMemberClass;
    return *this;
}

CompositeClass::CompositeClass(CompositeClass&& other) noexcept
    : BaseClass(std::move(other)),
      memberClass(std::move(other.memberClass)),
      anotherMemberClass(std::move(other.anotherMemberClass)) {
    std::cout << "CompositeClass move constructor" << std::endl;
}

CompositeClass& CompositeClass::operator=(CompositeClass&& other) noexcept {
    std::cout << "CompositeClass move assignment operator" << std::endl;
    BaseClass::operator=(std::move(other));
    memberClass = std::move(other.memberClass);
    anotherMemberClass = std::move(other.anotherMemberClass);
    return *this;
}

CompositeClass::~CompositeClass() {
    std::cout << "CompositeClass destructor" << std::endl;
}
