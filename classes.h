#ifndef DARKMATTER_CLASSES_H
#define DARKMATTER_CLASSES_H

//TODO: Can you shorten this code using templates/macros

class SimpleClass {
public:
    SimpleClass();
    SimpleClass(SimpleClass const&);
    SimpleClass& operator=(SimpleClass const&);
    SimpleClass(SimpleClass&&) noexcept;
    SimpleClass& operator=(SimpleClass&&) noexcept;
    ~SimpleClass();
};

class BaseClass {
public:
    BaseClass();
    BaseClass(BaseClass const&);
    BaseClass& operator=(BaseClass const&);
    BaseClass(BaseClass&&) noexcept;
    BaseClass& operator=(BaseClass&&) noexcept;
    ~BaseClass();
};

class AnotherBaseClass {
public:
    AnotherBaseClass();
    AnotherBaseClass(AnotherBaseClass const&);
    AnotherBaseClass& operator=(AnotherBaseClass const&);
    AnotherBaseClass(AnotherBaseClass&&) noexcept;
    AnotherBaseClass& operator=(AnotherBaseClass&&) noexcept;
    ~AnotherBaseClass();
};

class DerivedClass : public BaseClass, AnotherBaseClass {
public:
    DerivedClass();
    DerivedClass(DerivedClass const&);
    DerivedClass& operator=(DerivedClass const&);
    DerivedClass(DerivedClass&&) noexcept;
    DerivedClass& operator=(DerivedClass&&) noexcept;
    ~DerivedClass();
};

class MemberClass {
public:
    MemberClass();
    MemberClass(MemberClass const&);
    MemberClass& operator=(MemberClass const&);
    MemberClass(MemberClass&&) noexcept;
    MemberClass& operator=(MemberClass&&) noexcept;
    ~MemberClass();
};

class AnotherMemberClass {
public:
    AnotherMemberClass();
    AnotherMemberClass(AnotherMemberClass const&);
    AnotherMemberClass& operator=(AnotherMemberClass const&);
    AnotherMemberClass(AnotherMemberClass&&) noexcept;
    AnotherMemberClass& operator=(AnotherMemberClass&&) noexcept;
    ~AnotherMemberClass();
};

class CompositeClass : public BaseClass {
public:
    CompositeClass();
    CompositeClass(CompositeClass const&);
    CompositeClass& operator=(CompositeClass const&);
    CompositeClass(CompositeClass&&) noexcept;
    CompositeClass& operator=(CompositeClass&&) noexcept;
    ~CompositeClass();
    MemberClass memberClass;
    AnotherMemberClass anotherMemberClass;
};

#endif //DARKMATTER_CLASSES_H
