#include "classes.h"

#include <iostream>

//how many member functions does an "empty" class have? 4 (pre-c++11), 6 (c++11+)
class MyClass {
    //special member functions
    MyClass() = default; //default constructor
    ~MyClass() = default; //destructor
    MyClass(MyClass const&) = default; //copy constructor
    MyClass& operator=(MyClass const&) = default; //copy assignment operator
    //move semantics
    MyClass(MyClass&&) = default; //move constructor
    MyClass& operator=(MyClass&&) = default; //move assignment operator
    //rule of 0/5
    //rule of 6
};

int main() {
    //Default constructor and destructor
    {
        std::cout << "Calling the default constructor:" << std::endl;
        SimpleClass simpleClass;
        SimpleClass simpleClass1{};
        SimpleClass simpleClass2(); //function declaration
        SimpleClass simpleClass3 = SimpleClass{};
        SimpleClass simpleClass4 = SimpleClass();
        SimpleClass* simpleClass5 = new SimpleClass{};
        SimpleClass* simpleClass6 = new SimpleClass();
        SimpleClass* simpleClass7 = new SimpleClass;
        std::cout <<"\nCalling the destructor:" << std::endl;
    }

    {
        std::cout << "\n\nCalling the default constructor of a derived class:" << std::endl;
        DerivedClass derivedClass;
        std::cout <<"\nCalling the destructor:" << std::endl;
    }

    {
        std::cout << "\n\nCalling the default constructor of a composite class:" << std::endl;
        CompositeClass compositeClass;
        std::cout <<"\nCalling the destructor:" << std::endl;
    }

    //when does the compiler generate the default constructor? what about the destructor

    //Copy constructor and assignment operator
    {
        std::cout << "\n\nCalling the copy constructor:" << std::endl;
        std::cout << "\nConstructor:" << std::endl;
        CompositeClass original;
        std::cout << "\nCopy constructor:" << std::endl;
        CompositeClass copy{original};
        std::cout << "\nAssignment:" << std::endl;
        copy = original;
        std::cout << "\nSurprisingly:" << std::endl;
        CompositeClass anotherCopy = original;
        std::cout << "\nSlice:" << std::endl;
        BaseClass slice{original};
        std::cout << "\nDestructors:" << std::endl;
    }

    //move constructor
    {
        std::cout << "\nConstructor:" << std::endl;
        CompositeClass movedFrom;
        std::cout << "\nMove constructor:" << std::endl;
        CompositeClass movedTo = std::move(movedFrom);
        std::cout << "\nDestructors:" << std::endl;
    }

    return 0;
}

void homework() {
    //consider this class hierarchy:
    class A {};
    class B : A {};
    class C : B {};
    class D {};
    class E {};
    class F : D, C {
        E e;
    };
    //what is the construction order if we run the next line?
    F f{};
    //write code that calls F's move constructor and move assignment operator
    //bonus: can you do it without std::move?
}

void homework1() {
    //which special member functions do these lines call?
    std::cout << "\nWhat the F#:" << std::endl;
    CompositeClass wtf{CompositeClass{}}; //looks like it would call the move constructor, but doesn't
    std::cout << "\nNow we're asking for it:" << std::endl;
    CompositeClass lmao{CompositeClass{CompositeClass{CompositeClass{}}}};
}

void homework2() {
    //go to classes.cpp
    //check the special member functions marked as TODO
    //can you write them to be the equivalent to the compiler generated ones?
}

void homework3() {
    //a) why are the move constructors and move assignment operators marked as noexcept?
    //b) why aren't the other member functions marked as noexcept?
    //c) what happens if we mark a constructor as explicit?
    //d) what happens if we mark the move and copy constructors as explicit?
    //e) when and why should you mark the destructor as virtual? go to classes.h and do this
}

void homework4() {
    //a) what is the rule of 6?
    //b) what is the rule of 7?
}